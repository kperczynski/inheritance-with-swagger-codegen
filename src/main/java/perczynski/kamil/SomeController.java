package perczynski.kamil;

import io.swagger.model.MbaOrderLineDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Validated
public class SomeController {

    @PostMapping("/line")
    public MbaOrderLineDto some(@RequestBody @Valid MbaOrderLineDto line) {
        System.out.println(line);
        return line;
    }

}
